#include <iostream>
#include <string>
#include <ctime>
using namespace std;

struct  Dane    //przechowujemy imie i nr "biletu"
{
   string imie;
   int nr;
};

struct  Element     //wezel listy
{
    Dane  _dana;
    Element *nastepny;
};
class Kolejka
{
    Element *glowa;
    Element *ogon;      
    public:
    Kolejka()           //konstruktor - brak elementow = wskazniki na nic nie wskazuja
    {
         glowa=nullptr;
         ogon=nullptr;
    }
    void enqueue( Dane _dana);  //dodaj do kolejki
    void dequeue();             //usun z kolejki
    void wypisz();              //wypisz wszystkich w kolejce
    int size();                 //wielkosc kolejki
    void front();               //zwroc pierwsza osobe w kolejce
};

void Kolejka::enqueue( Dane _dana)      
{
    Element *nowy=new Element;      //tworzymy wskaznik na nowy wezel
    nowy->_dana=_dana;              //przypisujemy nowemu wezlowi nowa dane
     nowy->nastepny=nullptr;        //nowy wezel wskazuje na null
    if(ogon==nullptr)
    {
        ogon=nowy;                  // nie bylo jeszcze elementow na kolejce wiec ogon staje sie pierwszy elementem jaki stworzylismy
    }
    else
    {
 ogon->nastepny=nowy;              //poprzedni ostatni element wskazuje na nowo dodany (teraz ostatni) element 
 ogon=nowy;                         
   
    }
    if(glowa==nullptr) // sprawdzamy czy poczatek kolejki jest pusty, jesli tak, to przypisujemy mu tez element z ogona,
                        // bo mamy tylko 1 element ktory jednoczesnie jest koncem i poczatkiem
           glowa=ogon;
}

int Kolejka::size()    //tworzymy wskaznik, przesuwamy sie nim po calej liscie i zwracamy wartosc rozmiar, gdy dojdziemy do konca
{
     Element *nowy; 
     int roz=0;
    for(nowy=glowa; nowy; nowy=nowy->nastepny,roz++);
   return roz;
}


void Kolejka::dequeue( )      
{
    if (glowa==nullptr){            //sprawdzamy czy poczatek kolejki jest pusty, jesli tak, to zwracamy komunikat o bledzie
    cout << "BLAD! - NIKOGO NIE MA W KOLEJCE!" << endl;
    }
    else{
    Element *nowy=glowa;            //wskaznik wskazujacy na to, na co wskazywala glowa (pierwszy element) 
    glowa=glowa->nastepny;          //uaktualniamy glowe, by wskazywala na nastepny element
    delete(nowy);                   //usuwamy stara glowe
}
}

void Kolejka::wypisz()    
{
     Element *nowy;                                 //tworzymy wskaznik i przesuwamy sie nim po calej liscie, wypisujac wartosci w kazdym wezle
    for(nowy=glowa; nowy ; nowy=nowy->nastepny)     //zasada dzialania dokladnie taka jak funkcja size(), ale wypisujemy dane, zamiast zwiekszac numer rozmiaru
    {
        cout<< "Numer biletu: " <<nowy->_dana.nr<<endl;
        cout<< "Imie: " <<nowy->_dana.imie<<endl;
        cout<<"////////////////////////////"<<endl;
    }
}

void Kolejka::front()           //zwraca pierwszy element. !NIE USUWA GO!
{ if (glowa==nullptr){          //nikogo nie ma w kolejce? Tak jak dla dequeue(), stosowny komunikat
    cout <<"NIKOGO NIE MA W KOLEJCE" << endl;
    }
    else{
    Element *nowy=glowa;                             //wskaznik wskazujacy na pierwszy element

    cout<< "Na poczatku kolejki czeka :"<<endl;
    cout<< "Numer biletu: " <<nowy->_dana.nr<<endl;
    cout<< "Imie: " <<nowy->_dana.imie<<endl;
    cout<<"////////////////////////////"<<endl;
    }
}



int main(){
  
Kolejka nowa;       //tworzymy kolejke
string imiona[]={"Ala","Michal","Jozef","Alina","Monika","Mateusz","Kacper"};       //kilka imion dla pokazania dzialania programu
srand(time(NULL));  //losowy numer
    int wybor;
   

while (wybor!=5){       //menu glowne kolejki
   
        cout<<endl;
        cout<<endl;
        cout<<endl;
        cout << "MENU GLOWNE KOLEJKI:"<<endl;
        cout << "___________________________________________"<<endl;
        cout << "1. Dodaj osobe na koniec kolejki"<<endl;
        cout << "2. Usun osobe z poczatku kolejki - sprawa zostala zalatwiona"<<endl;
        cout << "3. Kto czeka w kolejce? "<<endl;
        cout << "4. Kto czeka na poczatku? "<<endl;
        cout << "5. Koniec programu "<<endl;
        cout<<endl;
        cout << "___________________________________________"<<endl;
        cout << "Aktualnie w kolejce czeka : "<< nowa.size()<<" osob"<<endl;
        cout << "___________________________________________"<<endl;
        cout<<endl;
        cout<<endl;
        cout<<endl;
        cout << "Wybor: ";
        cin >> wybor;

        switch (wybor)
        {
        case 1:
        nowa.enqueue( { imiona[rand()%(sizeof(imiona)/sizeof(string))],nowa.size()+1 });    //wybieramy losowo jedno z uprzednio stworzonych imion (ilosc elementow)/(dlugosc elementu)
            break;          //takiej osobie nadajemy numer "biletu". NIE moze sie on zmieniac!

        case 2:
           nowa.dequeue();
            break;

        case 3:
            nowa.wypisz();
            break;

         case 4:
            nowa.front();
            break;
        }

}

}

/*    TEST PROGRAMU - dodanie, usuniecie osoby, sprawdzenie zabezpieczenia, gdy nikogo nie ma

MENU GLOWNE KOLEJKI:
___________________________________________
1. Dodaj osobe na koniec kolejki
2. Usun osobe z poczatku kolejki - sprawa zostala zalatwiona
3. Kto czeka w kolejce?
4. Kto czeka na poczatku?
5. Koniec programu

___________________________________________
Aktualnie w kolejce czeka : 0 osob
___________________________________________



Wybor: 1



MENU GLOWNE KOLEJKI:
___________________________________________
1. Dodaj osobe na koniec kolejki
2. Usun osobe z poczatku kolejki - sprawa zostala zalatwiona
3. Kto czeka w kolejce?
4. Kto czeka na poczatku?
5. Koniec programu

___________________________________________
Aktualnie w kolejce czeka : 1 osob
___________________________________________



Wybor: 1



MENU GLOWNE KOLEJKI:
___________________________________________
1. Dodaj osobe na koniec kolejki
2. Usun osobe z poczatku kolejki - sprawa zostala zalatwiona
3. Kto czeka w kolejce?
4. Kto czeka na poczatku?
5. Koniec programu

___________________________________________
Aktualnie w kolejce czeka : 2 osob
___________________________________________



Wybor: 3
Numer biletu: 1
Imie: Monika
////////////////////////////
Numer biletu: 2
Imie: Ala
////////////////////////////



MENU GLOWNE KOLEJKI:
___________________________________________
1. Dodaj osobe na koniec kolejki
2. Usun osobe z poczatku kolejki - sprawa zostala zalatwiona
3. Kto czeka w kolejce?
4. Kto czeka na poczatku?
5. Koniec programu

___________________________________________
Aktualnie w kolejce czeka : 2 osob
___________________________________________



Wybor: 2



MENU GLOWNE KOLEJKI:
___________________________________________
1. Dodaj osobe na koniec kolejki
2. Usun osobe z poczatku kolejki - sprawa zostala zalatwiona
3. Kto czeka w kolejce?
4. Kto czeka na poczatku?
5. Koniec programu

___________________________________________
Aktualnie w kolejce czeka : 1 osob
___________________________________________



Wybor: 4
Na poczatku kolejki czeka :
Numer biletu: 2
Imie: Ala
////////////////////////////



MENU GLOWNE KOLEJKI:
___________________________________________
1. Dodaj osobe na koniec kolejki
2. Usun osobe z poczatku kolejki - sprawa zostala zalatwiona
3. Kto czeka w kolejce?
4. Kto czeka na poczatku?
5. Koniec programu

___________________________________________
Aktualnie w kolejce czeka : 1 osob
___________________________________________



Wybor: 3
Numer biletu: 2
Imie: Ala
////////////////////////////



MENU GLOWNE KOLEJKI:
___________________________________________
1. Dodaj osobe na koniec kolejki
2. Usun osobe z poczatku kolejki - sprawa zostala zalatwiona
3. Kto czeka w kolejce?
4. Kto czeka na poczatku?
5. Koniec programu

___________________________________________
Aktualnie w kolejce czeka : 1 osob
___________________________________________



Wybor: 2



MENU GLOWNE KOLEJKI:
___________________________________________
1. Dodaj osobe na koniec kolejki
2. Usun osobe z poczatku kolejki - sprawa zostala zalatwiona
3. Kto czeka w kolejce?
4. Kto czeka na poczatku?
5. Koniec programu

___________________________________________
Aktualnie w kolejce czeka : 0 osob
___________________________________________



Wybor: 2
BLAD! - NIKOGO NIE MA W KOLEJCE!



MENU GLOWNE KOLEJKI:
___________________________________________
1. Dodaj osobe na koniec kolejki
2. Usun osobe z poczatku kolejki - sprawa zostala zalatwiona
3. Kto czeka w kolejce?
4. Kto czeka na poczatku?
5. Koniec programu

___________________________________________
Aktualnie w kolejce czeka : 0 osob
___________________________________________



Wybor: 4
NIKOGO NIE MA W KOLEJCE



MENU GLOWNE KOLEJKI:
___________________________________________
1. Dodaj osobe na koniec kolejki
2. Usun osobe z poczatku kolejki - sprawa zostala zalatwiona
3. Kto czeka w kolejce?
4. Kto czeka na poczatku?
5. Koniec programu

___________________________________________
Aktualnie w kolejce czeka : 0 osob
___________________________________________

*/


